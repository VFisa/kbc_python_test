__author__ = "Martin Fiser"
__credits__ = "Martin Fiser, 2017, Twitter: @VFisa"


# Import Libraries
import os
import pip
import sys
import json
import pandas as pd
import numpy as np
import logging
from os import listdir
from os.path import isfile, join
from keboola import docker


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(filename='python_job.log',level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")



def load_parameters(specs):
    """
    Load default parameters from "config.json" file.
    Test input parameters
    """

    # load file
    try:
        with open("config.json", "r") as param_file:
            params = json.load(param_file)
    except Exception as e:
        logging.error("Could not load param file: 'config.json'" + str(e))
        sys.exit(1)

    # test input parameters
    try:
        for a in specs:
            test = params[a]
            logging.info("Input parameter " + str(a) + ": " + str(test))
    except Exception as e:
        logging.error("Could not load all parameters required. Exit.")
        sys.exit(1)

    return params


def load_tables(in_tables, out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["destination"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    return in_name, out_name


def input_file(file_in):
    """
    Read input data as CSV DataFrame (pandas)
    """

    try:
        frame = pd.read_csv(file_in, encoding="utf-8")
        logging.info("Input file read.")
    except Exception as e:
        logging.error("Could not read input file! Exit")
        logging.error(str(e))
        sys.exit(1)

    return frame




# Get proper list of tables
cfg = docker.Config('/data/')

params = cfg.get_parameters()
print(params)
column_date = cfg.get_parameters()["column_date"]
column_values = cfg.get_parameters()["column_values"]
period = cfg.get_parameters()["predict_period"]
specs = [column_date, column_values, period]
# access the supplied values, put this into parameter window:
"""
{
    "column_date": "string",
    "column_values": "string",
    "predict_period": 30
}
"""

in_link = "/data/in/tables/"
onlyfiles = [f for f in listdir(in_link) if isfile(join(in_link, f))]
print(onlyfiles)
intable = ("/data/in/tables/"+str(onlyfiles[0]))
print(intable)


in_tables = cfg.get_input_tables()
print(in_tables)
out_tables = cfg.get_expected_output_tables()
print(out_tables)
# determine data destination
#file_in, file_out = load_tables(in_tables, out_tables)
"""
"""

# Get data
model = input_file(intable)
# test print
print(model.head())


logging.info("Script completed.")
